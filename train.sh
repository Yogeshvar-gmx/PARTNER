python src/train_model.py \
	--model_name_or_path /Users/mags/Research/PARTNER/results/output-reddit \
	--train_input_file /Users/mags/Research/PARTNER/dataset/sample_data.128len.db \
	--output_dir output/ \
	--log_dir output/ \
	--train_batch_size 4 \
	--num_optim_steps 500